(ns plf06.core
  (:gen-class))

(def abecedario
  (seq "0123456789!\"#$%+'(),&-./;:<=>?@[/]^`{|}~aábcdeéfghiíjklmnñoópqrstuúüvwxyzAÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZ0123456789!\"#$%+'(),&-./;:<=>?@[/]^`{|}~"))

(defn rot-13
  [xs]
  (let [f (zipmap abecedario (drop 13 (take 147 (conj abecedario))))]
    (apply str (replace f xs))))

(defn -main
  [& args]
  (if (empty? args)
    (println "No se ha ingresado ningun argumento")
    (println (rot-13 (apply str args)))))